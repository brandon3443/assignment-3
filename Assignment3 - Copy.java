/* Name/Date/File/Desc.
Author:			Brian Chrysler & Brandon Miller
Date:			Nov. 6th 2015
File:			Assignment3.java
Description:	Program reads an infile, and calculates which debt to pay off in a rapid debt reduction style. Displays highest interest and lowest interes payments, once paid off,
				and apply it to the next highest interest,lowest payment and so on.
*/
/* debt.txt
MasterCard,Revolving Credit Card,2562.67,18.8,03
Canadian Tire,Revolving Credit Card,2782.32,25.99,03
Sears,Revolving Credit Card,1437.55,25.99,03
CIBC,Convertible Student Loan,14322.48,6.525,01
TD,Car Loan,8496.91,10.2,154.78
*/
/*

              _
                           _ooOoo_
                          o8888888o
                          88" . "88
                          (| -_- |)
                          O\  =  /O
                       ____/`---'\____
                     .'  \\|     |//  `.
                    /  \\|||  :  |||//  \
                   /  _||||| -:- |||||_  \
                   |   | \\\  -  /'| |   |
                   | \_|  `\`---'//  |_/ |
                   \  .-\__ `-. -'__/-.  /
                 ___`. .'  /--.--\  `. .'___
              ."" '<  `.___\_<|>_/___.' _> \"".
             | | :  `- \`. ;`. _/; .'/ /  .' ; |
             \  \ `-.   \_\_`. _.'_/_/  -' _.' /
   ===========`-.`___`-.__\ \___  /__.-'_.'_.-'================
                           `=--=-'                    
*/
/* ANALYSIS					
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Functionality: 	the user will input the path for the input and output file. the progam will populate an array with the infile data, error check for missing info from the infile,
				and numbers are parsed into doubles or ints (respectively). the rate,loanName, and payment will be populated into an arraylist.the program calculates the interest paid
				EACH yesr PER CARD, monthly payment. It will display the rates, display montly payments, and loan institutions (all of which on one line per for analysis). It will determine
				which card has lowest and highest rate, highest and lowest payments, numRecords processed, monthly payments and how much money you have left to apply to highest interest and
				lowesrt payment.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

TEST DATA:

INPUTS:													OUTPUTS:
MasterCard		-> $2562.67 	18.800% 	3.00%		2562.67  *.1880		= $481.78 | 2562. 67 * .0300 = $ 76.88
CanadianTire	-> $2782.32 	25.990% 	3.00%		2782.32  *.25990	= $723.12 | 2782. 32 * .0300 = $ 83.47
Sears			-> $1437.55 	25.990% 	3.00%		1437.55  *.25990	= $373.62 | 1437. 55 * .0300 = $ 43.13
CIBC			-> $14322.48	 6.525%		1.00%		14322.48 *.06525	= $934.54 | 14322.48 * .0100 = $143.22
TD				-> $8496.91		10.200%		-----		8496.91  *.10200	= $866.68 | 8496. 91 * ----- = $154.22

number of records:	5
highest rate: 		25.99%
lowest rate:		6.53%
highest payment:	$154.78
lowest payment:		$43.13
total monthly:		$501.48

monthly income: 	737.5
30% of income is for debt servicing, $221.25
currently at 68% of salary
will have $236.02 left to start paying highest interest


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Inputs:
	
	
	String loanName
    String loanType
	String amountIn
	String rateIn
	String paymentPercentIn
    double amount
    double rate
	
Outputs:

	String loanName
    String loanType
	String amountIn
	String rateIn
	String paymentPercentIn
	double paymentPercent 2dp
	double payment 2dp
	double interestPerYear 3dp
	double totalPayments 2dp
	double highRate 2dp
	double lowRate 2dp
	double highPmt 2dp
	double lowPmt 2dp
		
	double income 2dp
	double debtServicing 2dp
	double overUnder 2dp
	double balance 2dp
    int numRecords

	aryRates = new ArrayList<Double>();
	aryPayments = new ArrayList<Double>();
	aryNames = new ArrayList<String>();


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ALGORITHM:

P+G outfile name/path
P+G infile name/path
print and display date/names
	while(line!=null)
		populate array
		
		for i=0;i<nextfield.length;i++
			if nextfield[i].isWhitespace==true 
				error
				System.exit(-1);
			
			else
				assigning array value to double
				substring for title case then concatenate
		
		if loanName.compareTo("TD")==0  //TD is a payment, not % of balance
			display loanName
			display loanType
			display amount
			display rate
			display paymentPercent
			\n
			
		else
			calculate payment
			calculate interestPerYear
			
			display loanName
			display loanType
			display amount
			display rate
			display paymentPercent
			display interestPerYear
			display payment
			\n
			
		aryNames.add(nextfield[0]);
		aryPayments.add(payment);
		aryRates.add(nextfield[3]);

		numRecords++
		
		line=infile.readLineFromFile();
		
		if rate!=0
		
			if rate >highRate
			
				highRate=rate
				highRateLoanName = loanName
			
			if rate <lowRate
			
				lowRate = rate
				lowRateLoanName = loanName
			
		if payment!=0
			
			if payment > highPmt
				highPmt = payment
				highPmtLoanName = loanName
			
			if payment < lowPmt
				lowPmt = payment
				lowPmtLoanName = loanName
			
			totalPayments+= payment
			line = infile.readLineFromFile ()
		
		Display & Print highRate,highRateLoanName
		Display & Print lowRate,lowRateLoanName
		Display & Print highPmt, highPmtLoanName
		Display & Print lowPmt, lowPmtLoanName
		Display & Print totalPayments
		
		P&G income
		calculate debt servicing
		Display & Print debtServicingPercent, debtServicing
		

print & display numRecords
print & display aryRates
print & display aryPayments
print & display aryNames



			
		
*/

import java.util.*;
import java.text.SimpleDateFormat;
import java.io.*;

public class Assignment3
{
	public static void main (String[]args)
	{
		String loanName=null;
		String highRateLoanName=null;
		String lowRateLoanName=null;
		String highPmtLoanName=null;
		String lowPmtLoanName=null;
		String loanType=null;
		String amountIn=null;
		String rateIn=null;
		String paymentPercentIn=null;
		double amount=0;
		double rate=0;

		ArrayList<Double> aryRates = new ArrayList<Double>();
		ArrayList<Double> aryPayments = new ArrayList<Double>();
		ArrayList<String> aryNames = new ArrayList<String>();
		
		double paymentPercent=0;
		double payment=0;
		double interestPerYear=0;
		double totalPayments=0;
		double highRate=0;
		double lowRate=10000;
		double highPmt=0;
		double lowPmt=10000;
			
		double income=0;
		double incomePercent=0;
		double debtServicing=0;
		double overUnder=0;
		double balance=0;
		int numRecords=0;    
		int counter=0;
		double debtServicingPercent = 30;
		int index = 0;
		
		Scanner sc = new Scanner(System.in);
		String Out;
		String In;
		System.out.print("Please enter the path and file to which you wish to save the output: ");
		Out = sc.nextLine();
		UtilityClass outfile = new UtilityClass(Out);   	
		outfile.openFile(); 								
		
		System.out.print("Please enter the path and file from which you wish to access data: ");
		In = sc.nextLine();
		UtilityClassIn infile = new UtilityClassIn(In);  	
		infile.openInFile(); 								
	   
		String date = outfile.myDate();
		System.out.println(date);
		outfile.writeLineToFile(date);
		String name = outfile.myName();
		System.out.println(name);
		outfile.writeLineToFile(name);						
		
		Scanner inputFile=new Scanner(In);
		String line=infile.readLineFromFile();  

		while (line !=null)
		{
			String[] nextfield=line.split(",");
			
			for(int i = 0; i<nextfield.length; i++)
			{
				if(nextfield[0].equals(""))
				{
					System.out.printf("Error. Blank field at index 0 which is loan name");
					System.exit(-1); 
				}
				if(nextfield[1].equals(""))
				{
					System.out.printf("Error. Blank field at index 1 which is loan type");
					System.exit(-1); 
				}
				if(nextfield[2].equals(""))
				{
					System.out.printf("Error. Blank field at index 2 which is loan amount");
					System.exit(-1); 
				}
				if(nextfield[3].equals(""))
				{
					System.out.printf("Error. Blank field at index 3 which is interest rate");
					System.exit(-1); 
				}
				if(nextfield[4].equals(""))
				{
					System.out.printf("Error. Blank field at index 4 payment rate");
					System.exit(-1); 
				}					
				else						
				{	
					loanName=nextfield[0];   						
					loanType=nextfield[1]; 							
					index = loanType.indexOf(" ");
					String sub1 = loanType.substring(index);
					sub1 = sub1.toLowerCase();
					String sub2 = loanType.substring(0,index);
					loanType = sub2 + sub1;
				
					amountIn=nextfield[2];  
					rateIn=nextfield[3]; 	   
					paymentPercentIn=nextfield[4]; 
				}
			} //end for loop	
		
			amount = Double.parseDouble(amountIn);
			rate=Double.parseDouble(rateIn);
			paymentPercent=Double.parseDouble(paymentPercentIn);
			
			if (loanName.compareTo("TD")==0)
			{
				payment = paymentPercent;
				interestPerYear = amount *(rate / 100);
				System.out.printf("Loan Institution: %s \n",loanName);
				System.out.printf("Loan Type: %s\n",loanType);
				System.out.printf("Loan Amount: $%.2f\n" ,amount);
				System.out.printf("Interest Rate: %.3f%%\n" ,rate);
				System.out.printf("Interest Paid Per year: $%.2f\n", interestPerYear);				
				System.out.printf("Payment Amount: $%.2f\n" ,paymentPercent);
				System.out.println();

				outfile.writeLineToFile("Loan Institution: %s \n",loanName);
				outfile.writeLineToFile("Loan Type: %s\n",loanType);
				outfile.writeLineToFile("Loan Amount: $%.2f\n" ,amount);
				outfile.writeLineToFile("Interest Rate: %.3f%%\n" ,rate);
				outfile.writeLineToFile("Interest Paid Per year: $%.2f\n", interestPerYear);				
				outfile.writeLineToFile("Payment Amount: $%.2f\n" ,paymentPercent);
				outfile.writeLineToFile("\r\n");			
			}	
			else 
			{
				payment = amount * (paymentPercent / 100);
				interestPerYear = amount *(rate / 100);
				System.out.printf("Loan Institution: %s \n",loanName);
				System.out.printf("Loan Type: %s\n",loanType);
				System.out.printf("Loan Amount: $%.2f\n" ,amount);
				System.out.printf("Interest Rate: %.3f%%\n" ,rate);		
				System.out.printf("Payment Rate: %.2f%%\n" ,paymentPercent);
				System.out.printf("Interest Paid Per year: $%.2f\n", interestPerYear);
				System.out.printf("Payment Per Month $%.2f\n",payment);
				System.out.println();
				
				outfile.writeLineToFile("Loan Institution: %s \n",loanName);
				outfile.writeLineToFile("Loan Type: %s\n",loanType);
				outfile.writeLineToFile("Loan Amount: $%.2f\n" ,amount);
				outfile.writeLineToFile("Interest Rate: %.3f%%\n" ,rate);		
				outfile.writeLineToFile("Payment Rate: %.2f%%\n" ,paymentPercent);
				outfile.writeLineToFile("Interest Paid Per year: $%.2f\n", interestPerYear);
				outfile.writeLineToFile("Payment Per Month $%.2f\n",payment);
				outfile.writeLineToFile("\r\n");
				
			}
			
			aryRates.add(rate);
			aryPayments.add(payment);
			aryNames.add(loanName);
			
			if (rate!= 0)
			{
				if(rate > highRate)
				{
					highRate = rate;
					highRateLoanName = loanName;
				}
				if(rate < lowRate)
				{
					lowRate = rate;
					lowRateLoanName = loanName;
					
				} 
			} // end rate high low
			
			if (payment != 0)
			{
				if(payment > highPmt)
				{
					highPmt = payment;
					highPmtLoanName = loanName;
				}
				if(payment < lowPmt)
				{
					lowPmt = payment;
					lowPmtLoanName = loanName;
				}
			}
			
			totalPayments += payment;
			line = infile.readLineFromFile();
			numRecords++;
		} //end while loop
		
		
		System.out.print("number of records processed is: "+numRecords+"\n");
		System.out.print("Rates are: "+aryRates+"\n");
		System.out.print("Payments are: "+aryPayments+"\n");
		System.out.print("Loan institutions are: "+aryNames+"\n\n");
		outfile.writeLineToFile("number of records processed is: "+numRecords+"\n");
		outfile.writeLineToFile("Rates are: "+aryRates+"\n");
		outfile.writeLineToFile("Payments are: "+aryPayments+"\n");
		outfile.writeLineToFile("Loan institutions are: "+aryNames+"\n\n");
		
		System.out.printf("Highest rate is %.2f%% for %s\n",highRate,highRateLoanName);
		outfile.writeLineToFile("Highest rate is %.2f%% for %s\n", highRate, highRateLoanName);
		System.out.printf("Lowest rate is %.2f%% for %s\n", lowRate, lowRateLoanName);
		outfile.writeLineToFile("Lowest rate is %.2f%% for %s\n", lowRate, lowRateLoanName);
		System.out.printf("Highest payment is $%.2f for %s\n", highPmt, highPmtLoanName);
		outfile.writeLineToFile("Highest payment is $%.2f for %s\n", highPmt, highPmtLoanName);
		System.out.printf("Lowest payment is $%.2f for %s\n", lowPmt, lowPmtLoanName);
		outfile.writeLineToFile("Lowest payment is $%.2f for %s\n", lowPmt, lowPmtLoanName);
		System.out.printf("Total monthly payments are $%.2f", totalPayments);
		outfile.writeLineToFile("Total monthly payments are $%.2f", totalPayments);
		
		System.out.println("\n\nEnter your monthy income: ");
		income = sc.nextDouble();
		debtServicing = income * (debtServicingPercent / 100);
		incomePercent=(totalPayments/income)*100;
		System.out.printf("Currently, %.2f%% of your income, which is the amount of money eligible for debt servicing, is $%.2f\n",debtServicingPercent, debtServicing);
		outfile.writeLineToFile("Currently, %.2f%% of your income, which is the amount of money eligible for debt servicing, is $%.2f\n",debtServicingPercent, debtServicing);
		
		if (incomePercent<=debtServicingPercent)
		{
			System.out.printf("Currently you are at %.2f%% of your salary, and living within your means.",incomePercent);
			outfile.writeLineToFile("Currently you are at %.2f%% of your salary, and living within your means.",incomePercent);
		}
		else
		{
			System.out.printf("\nCurrently you are at %.2f%% of your salary, and NOT living within your means.",incomePercent);
			outfile.writeLineToFile("Currently you are at %.2f%% of your salary, and NOT living within your means.",incomePercent);
		}
		
		balance=income-totalPayments;
		System.out.printf("\n\nYou will have $%.2f left over to start paying off your highest interest, lowest payment credit card\n",balance);
		outfile.writeLineToFile("\n\nYou will have $%.2f left over to start paying off your highest interest, lowest payment credit card\n",balance);
		
		// System.out.println("Index is " +i);		//uncomment this code and run this, so you can see what the index is doing
	   outfile.closeFile();
	   infile.closeInFile();
	   
	} //end main
} //end Assignment3 class